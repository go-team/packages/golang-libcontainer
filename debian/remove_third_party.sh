#!/bin/sh -e
# Remove references to locally bundled third party libraries.
# Instead, rely on the system ones.
# Written by Jelmer Vernooij <jelmer@debian.org>, 2014.

for I in $(cat debian/third_party_exclude)
do
	echo "Removing third party bundled $I"
	rm -r obj-*/src/github.com/docker/libcontainer/vendor/src/$I
done
